const db = require("../db");
const express = require("express");
const utils = require("../utils");
const { request, response } = require("express");

const router = express.Router();

router.post("/", (request, response) => {
  const { name, salary, age } = request.body;

  const query = `
  INSERT INTO EMP
  (name , salary , age) 
  VALUES('${name}','${salary}', '${age}')
  `;

  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/", (request, response) => {
  const query = `
    SELECT  
    empid, name , salary , age 
    FROM EMP`;
  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.put("/:id", (request, response) => {
  const { id } = request.params;
  const { name, salary, age } = request.body;

  const query = `
    UPDATE  EMP SET
    name ='${name}' , 
    salary ='${salary}' ,
    age =${age}
    WHERE empid = ${id}
  `;

  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.delete("/:id", (request, response) => {
  const { id } = request.params;
  const query = `
    DELETE FROM EMP
    WHERE empid = ${id}
  `;

  db.execute(query, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
