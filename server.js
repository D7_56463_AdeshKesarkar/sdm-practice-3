const express = require("express");

const app = express();

app.use(express.json());

const empRouter = require("./routes/emp");
app.use("/emp", empRouter);

app.listen(4000, "0.0.0.0", () => {
  console.log("emp server started at 4000");
});
